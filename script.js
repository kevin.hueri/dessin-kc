// Je récupère mon canvas.
const canvas = document.getElementById("app")
// je récupère le contexte - C'est ça qui va me donner les informations
// ça va me permettre de dessiner dans mon canva
const ctx = canvas.getContext("2d")
//je crée une variable. elle sera égal au document auquel je récupère l'id de l'élément - ici ça sera l'input color
var color = document.getElementById('color')
//je crée une variable. elle sera égal au document auquel je récupère l'id de l'élément - ici ça sera l'input number
var size = document.getElementById('size')
var cRect = canvas.getBoundingClientRect()
//cette variable va définir une grosseur/épaisseur par défaut du crayon
var brosse = 3
var defaultColor = "#000000"

var WIDTH = innerWidth - cRect.left
var HEIGHT  = innerHeight - cRect.top
var isDrawing = false

var positions = []

// Je définis ma couleur
color.addEventListener("change", function(evt) {
    defaultColor = evt.explicitOriginalTarget.value
})

// Je définis la taille de ma brosse
size.addEventListener("change", function (evt){
    brosse = evt.explicitOriginalTarget.value
})

// Je dessine au clic
canvas.addEventListener("mousedown", function(evt) {
    isDrawing = true
    ctx.beginPath()
    ctx.moveTo(evt.clientX, evt.clientY)
    drawing(evt)
})

// J'arrete le dessin a la fin du clic
canvas.addEventListener("mouseup", function(evt) {
    isDrawing = false
    send()
    path = []
})

// Je dessine en fonction de la couleur et de l'épaisseur du pinceau
function drawing(click) {
    ctx.strokeStyle = defaultColor
    ctx.lineWidth = brosse
    ctx.lineTo(click.clientX, click.clientY)
    ctx.stroke()
    canvas.addEventListener("mousemove", function(evt) {

        if (isDrawing){
            ctx.lineCap = "square"
            ctx.lineTo(evt.clientX, evt.clientY)
            ctx.stroke()
            let arr = [evt.clientX, evt.clientY]
            positions.push(arr)

        }
    })
}

$.ajax('https://api.draw.codecolliders.dev/paths').done(function(draws) {

    for (draw of draws) {
        ctx.beginPath()
        ctx.strokeStyle = draw.strokeColor
        ctx.lineWidth = draw.lineWidth
        ctx.moveTo(draw.path[0][0], draw.path[0][1])
        for (let i = 1; i < draw.path.length; i++) {
            ctx.lineTo(draw.path[i][0], draw.path[i][1])
        }
        ctx.stroke()
    }
})

var time = Date.now()

// J'envoie mes infos à l'API
function send() {
    $.ajax({
        url: 'https://api.draw.codecolliders.dev/paths/add',
        method: 'POST',
        data: {
            // createdAt: time,
            path: positions,
            strokeColor: defaultColor,
            lineWidth: brosse,
        }
    })
}


// if(isset('#checkbox')) {
//     var lastId = 0;
//     var timeout;
//     var delay = 600000;
//
//     function remove() {
//         $.ajax({
//             url: 'https://api.draw.codecolliders.dev/paths',
//             method: 'POST',
//             data: {
//                 createdAt:
//                 path: positions,
//                 strokeColor: color,
//                 lineWidth: brosse,
//             }
//         }).done(function () {
//             if (data.path)
//                 })
//     }
// }